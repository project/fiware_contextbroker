<?php
/*
* Page with a list of all times
*/
function contextbroker_list_types() {
  $types = contextbroker_get_types();  
  $header = array(t('Type'),t('Actions'));
  $rows = null;
  foreach($types as $key => $type){
    $actions = array(l(t('Details'),"admin/structure/contextbroker/type/$key"));
    $rows[] = array($key,implode(' ',$actions));
  }
  $output = theme('table', array('header' => $header,
                                 'empty' => t('No types found'),
                                 'rows' => $rows ));
  return $output;  
}

/*
* Page with details of a type
*/
function contextbroker_type_details($type) {
  drupal_set_title($type);
  $type = contextbroker_get_type_details($type);
  $header = array(t('Attributes'),t('Type'));
  foreach($type->attrs as $key => $type){
    $rows[] = array($key,$type->type);
  }
  $output = theme('table', array('header' => $header,
                                 'rows' => $rows ));
  $output .= l('Back to types','admin/structure/contextbroker/types');
  return $output;  
}


/*
* Page with a list of all entities
*/
function contextbroker_list_entities() {
  $entities = contextbroker_get_all_entities();
  $header = array(t('Id'),t('Type'),t('Actions'));
  $rows = null;
  if(isset($entities->contextResponses)) {
    $entities = $entities->contextResponses;  
    foreach($entities as $key => $entity){
      $id = $entity->contextElement->id;
      $actions = array(l(t('Details'),"admin/structure/contextbroker/entity/$id"),l(t('Delete'),"admin/structure/contextbroker/entity/$id/delete"));
      $rows[] = array($id, $entity->contextElement->type, implode(' ',$actions));
    }
  }
  $output = theme('table', array('header' => $header,
                                 'empty' => t('No entities found'),
                                 'rows' => $rows ));
  return $output;  
}

/*
* Page with details of a entity
*/
function contextbroker_entity_details($id) {
  drupal_set_title($id);
  $entity = contextbroker_get_entity($id);

  foreach($entity as $key => $value){
    $header[] = $key;
  }
  foreach($entity as $key => $value){
    if(is_object($value)) {
      $value = $value->value;
    }
    $row[] = $value;    
  }
  $rows[] = $row;

  $output = theme('table', array('header' => $header,
                                 'rows' => $rows ));
  $output .= l('Back to entities','admin/structure/contextbroker/entities');
  return $output;   
}

/*
* Page with list of all subscriptions
*/
function contextbroker_list_subscriptions() {
  $subscriptions = contextbroker_get_subscriptions();
  $header = array(t('ID'),t('expires'),t('Actions')); 
  $rows = null;
  foreach($subscriptions as $subscription) {
    $id = $subscription->id;
    $actions = array(l(t('Delete'),"admin/structure/contextbroker/subscription/$id/delete"),l(t('Details'),"admin/structure/contextbroker/subscription/$id"));
    $rows[] = array($subscription->id,$subscription->expires,implode(' ',$actions));
  }
  $output = l(t('Add subscription'),'admin/structure/contextbroker/subscriptions/add');
  $output .= theme('table', array('header' => $header,
                                 'empty' => t('No subscriptions found'),
                                 'rows' => $rows ));
  
  return $output;                                  
}

function contextbroker_subscription_details($id){
  $subscription = contextbroker_get_subscription_details($id);
  return print_r($subscription,true);
}

/*
* Form for adding subscriptions
*/
function contextbroker_add_subscriptions_form($form, &$form_state) {
  global $base_url;
  $entities = contextbroker_get_all_entities();
  $entities = $entities->contextResponses;
  $id_options[0] = t('Select');
  foreach($entities as $entity) {
    $id_options[$entity->contextElement->id] = $entity->contextElement->id;
  }
  $form['entity_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entitie and attributes')
  ); 
  $form['entity_wrapper']['subscriptions_entity_id'] = array(
    '#type' => 'select',
    '#options' => $id_options,
    '#default_value' => 0,
    '#required' => TRUE,
    '#title' => t('Entity id'),
    '#ajax' => array(
      'event' => 'change',
      'wrapper' => 'attr-wrapper',
      'callback' => 'contextbroker_add_subscriptions_id_change_ajax_callback',
      'method' => 'replace',
    ),    
  );

  $form['entity_wrapper']['subscriptions_attr'] = array(
    '#prefix' => '<div id="attr-wrapper">',
    '#suffix' => '</div>',
    '#type' => 'markup',   
  ); 
  $form['subscriptions_endpoint'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,    
    '#title' => t('Subscription reference'),   
    '#description' => t('Endpoint of the subscription e.g. @site/accumulate',array('@site' => $base_url)),   
  );  
  $form['subscriptions_duration'] = array(
    '#type' => 'textfield', 
    '#required' => TRUE,
    '#title' => t('Subscription duration'),   
    '#description' => t('Insert a ISO8601 formatted date. e.g. PT5S'),   
  );  
  $form['type_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notify conditions')
  );
  
  $type_options = array(0 => t('Select'),'ONTIMEINTERVAL' => 'ON TIME INTERVAL', 'ONCHANGE' => 'ON CHANGE');
  $form['type_wrapper']['subscriptions_type'] = array(
    '#type' => 'select',
    '#options' => $type_options,
    '#default_value' => 0,
    '#required' => TRUE,
    '#title' => t('Type of subscription'), 
    '#ajax' => array(
      'event' => 'change',
      'wrapper' => 'type-wrapper',
      'callback' => 'contextbroker_add_subscriptions_type_change_ajax_callback',
      'method' => 'replace',
    ),    
  ); 
  $form['type_wrapper']['subscriptions_type_wrapper'] = array(
    '#prefix' => '<div id="type-wrapper">',
    '#suffix' => '</div>',
    '#type' => 'markup',   
  ); 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('add'),
  );
  return $form;
 
}

function contextbroker_add_subscriptions_id_change_ajax_callback($form, &$form_state) {  
  $form_state['rebuild'] = TRUE;
  $subscriptions_entity_id = $form_state['values']['subscriptions_entity_id'];
  $entity = contextbroker_get_entity($subscriptions_entity_id);
  $type = contextbroker_get_type_details($entity->type);
  foreach($type->attrs as $key => $type){
      $options[$key] = $key;
  }
  foreach($options as $option){
    $form['subscriptions_attr'][$option]['#type'] = 'checkbox';  
    $form['subscriptions_attr'][$option]['#title'] = $option;  
    $form['subscriptions_attr'][$option]['#name'] = 'subscriptions_attr_'.$option;  
  }
  $form['subscriptions_attr']['#prefix'] = '<div id="attr-wrapper">';
  $form['subscriptions_attr']['#suffix'] = '</div>';
  return $form['subscriptions_attr'];
}

function contextbroker_add_subscriptions_type_change_ajax_callback($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  switch($form_state['input']['subscriptions_type']) {
    case 'ONCHANGE':
      $subscriptions_entity_id = $form_state['values']['subscriptions_entity_id'];
      $entity = contextbroker_get_entity($subscriptions_entity_id);
      $type = contextbroker_get_type_details($entity->type);
      foreach($type->attrs as $key => $type){
          $options[$key] = $key;
      }
      foreach($options as $option){
        $form['onchange']['#prefix'] = '<div id="type-wrapper">';
        $form['onchange']['#suffix'] = '</div>';
        $form['onchange'][$option]['#type'] = 'checkbox';  
        $form['onchange'][$option]['#title'] = $option;  
        $form['onchange'][$option]['#name'] = 'onchange_'.$option;  
      }      
      return $form['onchange'];
    break;
    case 'ONTIMEINTERVAL':
      $form['ontimeinterval']['#prefix'] = '<div id="type-wrapper">';
      $form['ontimeinterval']['#suffix'] = '</div>';
      $form['ontimeinterval']['#type'] = 'textfield';  
      $form['ontimeinterval']['#title'] = t('Interval');
      $form['ontimeinterval']['#name'] = 'ontimeinterval';
      $form['ontimeinterval']['#required'] = TRUE;
      
      $form['ontimeinterval']['#description'] = t('Insert a ISO8601 formatted date. e.g. PT5S');
      return $form['ontimeinterval'];
    break;
  }
  return '';
}

function contextbroker_add_subscriptions_form_submit($form, &$form_state) {
  //$data = array()
  $data['entity_id'] = $form_state['input']['subscriptions_entity_id'];
  foreach($form_state['input'] as $key => $input){    
    if(strpos($key, 'subscriptions_attr_') !== false){
      $temp = explode('_',$key);
      $data['attributes'][] = $temp[2];
    }
  }  
  $data['endpoint'] = $form_state['input']['subscriptions_endpoint'];
  $data['duration'] = $form_state['input']['subscriptions_duration'];
  $data['type'] = $form_state['input']['subscriptions_type'];
  foreach($form_state['input'] as $key => $value){
    if($key == strtolower($data['type'])) {
      $data[$key] = array($value);
    }
    elseif(strpos($key, strtolower($data['type'].'_')) !== false){
      $temp = explode('_',$key);
      $data['condValues'][] = $temp[1];
    }
  }
  $entity = contextbroker_get_entity($data['entity_id']);
  $data['entity_type'] = $entity->type;
  contextbroker_add_subscription($data);
  drupal_goto('admin/structure/contextbroker/subscriptions');
}