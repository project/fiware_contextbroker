<?php

function contextbroker_setup_form($form, &$form_state) {
  $form['contextbroker_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port number of the contextbroker'),
    '#default_value' => variable_get('contextbroker_port',null),    
  );
  $form['contextbroker_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host of the contextbroker'),
    '#description' => 'eg. 127.0.0.1',
    '#default_value' => variable_get('contextbroker_host',null),
  );    

  return system_settings_form($form);
}